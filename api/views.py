from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
import requests
from .models import UserKeyVault
from requests.auth import HTTPBasicAuth
import json
import os
from rest_framework.views import APIView

github_oauth_id = os.environ.get("GITHUB_OAUTH_CLIENT_ID")
github_oauth_secret = os.environ.get("GITHUB_OAUTH_CLIENT_KEY")


# Create your views here.
class GithubAuth(APIView):
    def get(self, request):
        try:
            code = request.GET.get('code')
            url = 'https://github.com/login/oauth/access_token'
            data = {
                'client_id': github_oauth_id,
                'client_secret': github_oauth_secret,
                'code': code,
            }
            auth_response = requests.post(url, data=data, headers={
                'Accept': 'application/json'
            })
            auth_json_response = auth_response.json()
            if auth_response.status_code == 200:
                auth_token = auth_json_response['access_token']
                user_response = requests.get('https://api.github.com/user',
                                             headers={
                                                 'Authorization': f'bearer {auth_json_response["access_token"]}'
                                             })
                user_name = user_response.json()['login']
                if UserKeyVault.objects.filter(user_name=user_name).exists():
                    user = UserKeyVault.objects.get(user_name=user_name)
                    user.user_token = auth_token
                    user.save()
                else:
                    user = UserKeyVault.objects.create(user_name=user_name, user_token=auth_token)
                    user.save()
                return JsonResponse(auth_json_response)
            else:
                return JsonResponse({'error': 'Invalid code'}, status=400)
        except Exception as error:
            return JsonResponse({'error': str(error)}, status=500)

    def delete(self, request):
        try:
            access_token = request.GET.get('access_token').split(' ')[1]
            delete_token = requests.delete("https://api.github.com/applications/a85a5776503ecf1df3c8/grant", headers={
                "Accept": "application/vnd.github+json",
                "X-GitHub-Api-Version": "2022-11-28",
            }, auth=HTTPBasicAuth(github_oauth_id, github_oauth_secret), data=json.dumps({
                "access_token": access_token
            }))
            if UserKeyVault.objects.filter(user_token=access_token).exists():
                user = UserKeyVault.objects.get(user_token=access_token)
                user.delete()
                return JsonResponse({'success': 'true'}, status=200)
            else:
                return JsonResponse({'message': 'user doesn\'t exist'}, status=400)
        except Exception as error:
            return JsonResponse({'error': str(error)}, status=500)
