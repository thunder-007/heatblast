from django.contrib import admin
from .models import UserKeyVault

# Register your models here.
admin.site.register(UserKeyVault)
