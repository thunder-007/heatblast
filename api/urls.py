from django.contrib import admin
from django.urls import path, include
from . import views
from django.http import HttpResponse

urlpatterns = [
    path('auth/github/', views.GithubAuth.as_view()),
    path('', lambda request: HttpResponse("Hello World")),
]
