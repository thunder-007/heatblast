from django.db import models


# Create your models here.

class UserKeyVault(models.Model):
    user_name = models.CharField(max_length=100, unique=True)
    user_token = models.CharField(max_length=100)

    def __str__(self):
        return self.user_name

    class Meta:
        verbose_name_plural = 'UserKeys'
        verbose_name = 'User_Key'
